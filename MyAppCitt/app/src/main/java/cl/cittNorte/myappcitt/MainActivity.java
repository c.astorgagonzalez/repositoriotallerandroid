package cl.cittNorte.myappcitt;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<String> nombresGenericos = new ArrayList<>();
    public RecyclerView mRecycleViewInfo;
    public RecyclerView.Adapter mAdapterInfo;
    public RecyclerView.LayoutManager mLayoutManagerInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();


    }

    private void init() {
        for (int i = 0; i < 10 ; i++) {
            nombresGenericos.add("Nombre"+i);
        }

        mRecycleViewInfo = findViewById(R.id.recylerViewCitt);
        mRecycleViewInfo.setHasFixedSize(true);

        mLayoutManagerInfo = new LinearLayoutManager(this);
        mRecycleViewInfo.setLayoutManager(mLayoutManagerInfo);

        mAdapterInfo = new recyclerviewAdapter(nombresGenericos, getApplicationContext(),this);
        mRecycleViewInfo.setAdapter(mAdapterInfo);
    }


}
