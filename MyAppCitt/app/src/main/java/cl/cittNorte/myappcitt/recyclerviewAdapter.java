package cl.cittNorte.myappcitt;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class recyclerviewAdapter extends  RecyclerView.Adapter<recyclerviewAdapter.ViewGenerica> {

    List<String> nombres;
    Context mContext;
    Activity mActivity;

    public recyclerviewAdapter(List<String> nombres, Context mContext, Activity mActivity) {
        this.nombres = nombres;
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public static class ViewGenerica extends RecyclerView.ViewHolder{

        TextView textoGenerico;
        Button btnGenerico;

        public ViewGenerica(@NonNull View itemView) {
            super(itemView);
            textoGenerico = itemView.findViewById(R.id.textGenerico);
            btnGenerico = itemView.findViewById(R.id.btnGenerico);
        }

        public void bindData(String texto, Context context, Activity activity){
            textoGenerico.setText(texto);
        }
    }

    @NonNull
    @Override
    public ViewGenerica onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item,parent,false);


        return new recyclerviewAdapter.ViewGenerica(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewGenerica holder, int position) {
        holder.bindData(nombres.get(position),mContext,mActivity);

        holder.btnGenerico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,"Estoy en la posicion "+holder.getAdapterPosition(),Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return nombres.size();
    }
}
